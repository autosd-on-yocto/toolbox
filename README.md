# AutoSD Yocto Toolbox

This toolbox image provide a function containerized environment to build a Yocto Poky
distribution using the AutoSD Layer.

## License

[MIT](./LICENSE)
